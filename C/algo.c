//
//  algo.c
//  C
//
//  Created by bullhead on 12/4/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
static int hex_to_ascii(char c, char d);
static int hex_to_int(char c);
int main()
{
    char buff[] = "1EFF060001092002EC903D746E5A6E3F6B55036BAD1";
    
    char *subbuff=(char *)calloc(11, sizeof(char));
    memcpy(subbuff,&buff[24], 10 );
    subbuff[10] = '\0';
    printf("sybBuff '%s' \n", subbuff);
    char rawWBandName[6];
    for(int i = 0; i < strlen(subbuff); i+=2)
    {
        rawWBandName[i/2] = hex_to_ascii(subbuff[i],subbuff[i+1]);
    }
    printf("rawWBandName '%s' \n",rawWBandName);
}
static int hex_to_int(char c){
    int first = c / 16 - 3;
    int second = c % 16;
    int result = first*10 + second;
    if(result > 9) result--;
    return result;
}

static int hex_to_ascii(char c, char d){
    int high = hex_to_int(c) * 16;
    int low = hex_to_int(d);
    return high+low;
}
