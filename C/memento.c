//
//  memento.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>
#include <stdlib.h>
int main(int argc, const char * argv[]){
    int *number=(int*)malloc(sizeof(int));
    *number=100;
    printf("Number is %d\n",*number);
    free(number);
    int num_numbers;
    printf("Input number of numbers: ");
    scanf("%d",&num_numbers);
    int *numbers=(int*)calloc(num_numbers, sizeof(int));
    for (int i=0; i<num_numbers;i++) {
        printf("Input number [%d]: ",i+1);
        scanf("%d",&numbers[i]);
    }
    printf("Numbers are: [");
    for (int i=0; i<num_numbers; i++) {
        if (i+1<num_numbers) {
            printf("%d,",numbers[i]);
        }else{
            printf("%d]\n",numbers[i]);
        }
    }
    free(numbers);
    numbers=NULL;
    printf("Input Number of people: ");
    scanf("%d",&num_numbers);
    char **persons=(char**)calloc(num_numbers, sizeof(char*));
    for (int i=0; i<num_numbers; i++) {
        persons[i]=(char*)calloc(10, sizeof(char));
        printf("%d. Input name: ",i+1);
        scanf("%s",persons[i]);
    }
    printf("\n");
    for (int i=0; i<num_numbers; i++) {
        printf("%d. Name is: %s\n",i+1,persons[i]);
    }
    return 0;
}
