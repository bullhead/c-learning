//
//  mac_parser.c
//  C
//
//  Created by bullhead on 1/12/21.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct wifi{
    char *name;
    int rssi;
    char *mac;
} Wifi;

int main(int argc, char **argv){
    char * buffer = 0;
    long length;
    FILE * f = fopen ("/Users/bullhead/IdeaProjects/C/C/macs", "r");
    
    if (f){
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buffer = malloc (length);
        if (buffer){
            fread (buffer, 1, length, f);
        }
        fclose (f);
    }else{
        printf("failed to read file.\n");
    }
    
    if(buffer){
        Wifi **wifis;
        char *line=strtok(buffer, "\n");
        int count=1;
        char **lines;
        //copy all the lines in lines array. later we can use strtok on each line
        while (line) {
            if (strcmp("AT+CWLAP", line)==0) {
                line=strtok(NULL, "\n");
            }else if(strcmp("OK", line)==0){
                break;
            }else{
                if(count==1){
                    lines=(char**)calloc(count, sizeof(char*));
                }else{
                    lines=realloc(lines, count*sizeof(char*));
                }
                lines[count-1]=(char *)calloc(strlen(line), sizeof(char));
                strcpy(lines[count-1], line);
                count++;
                line=strtok(NULL, "\n");
            }
        }
        
        wifis=(Wifi **)calloc(count-1, sizeof(Wifi *));
        for (int i=0; i<count-1; i++) {
            Wifi *wifi=(Wifi *)calloc(1, sizeof(Wifi));
            char *current_line=lines[i];
            strtok(current_line, ",");
            char *name=strtok(NULL, ",");
            wifi->name=(char *)calloc(strlen(name), sizeof(char));
            strcpy(wifi->name, name);
            wifi->rssi=atoi(strtok(NULL, ","));
            char *mac=strtok(NULL, ",");
            wifi->mac=(char *)calloc(strlen(mac), sizeof(char));
            strcpy(wifi->mac, mac);
            wifis[i]=wifi;
            free(current_line);
        }
        count=count-1;
        for(int i=0;i<count-1;i++){
            for(int j=0;j<count-i-1;j++){
                Wifi *current=wifis[j];
                Wifi *next=wifis[j+1];
                if(current->rssi < next->rssi){
                    wifis[j]=next;
                    wifis[j+1]=current;
                }
            }
        }
        
        for (int i=0; i<count; i++) {
            Wifi *wifi=wifis[i];
            printf("[%s]-[%d]-[%s]\n",wifi->name,wifi->rssi,wifi->mac);
            free(wifi);
        }
        free(lines);
        free(wifis);
     
    }
}
