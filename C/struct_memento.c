//
//  struct_memento.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINUX_LOGO_MONO     1   /* monochrome black/white */
#define LINUX_LOGO_VGA16    2   /* 16 colors VGA text palette */
#define LINUX_LOGO_CLUT224  3   /* 224 colors */
#define LINUX_LOGO_GRAY256  4   /* 256 levels grayscale */
#define LINUX_LOGO_bmp 5

static const char logo_types[LINUX_LOGO_bmp+1][100] = {
    [LINUX_LOGO_MONO] = "LINUX_LOGO_MONO",
    [LINUX_LOGO_VGA16] = "LINUX_LOGO_VGA16",
    [LINUX_LOGO_CLUT224] = "LINUX_LOGO_CLUT224",
    [LINUX_LOGO_GRAY256] = "LINUX_LOGO_GRAY256"
};
//or it can be

struct point {
    int x;
    int y;
};

int main(int argc,char ** argv){
    struct point *p=(struct point*)malloc(sizeof(struct point));
    p->x=10;
    p->y=10;
    printf("(x,y) = (%d,%d)\n",p->x,p->y);
    printf("Size of p is %lu\n",sizeof(p));
    free(p);
    
    char logo_types[LINUX_LOGO_bmp+1][30];
    
    strcpy(logo_types[1],"LINUX_LOGO_MONO");
    strcpy(logo_types[2], "LINUX_LOGO_VGA16");
    strcpy(logo_types[3], "LINUX_LOGO_CLUT224");
    strcpy(logo_types[4], "LINUX_LOGO_GRAY256");
    
    
    
    
    printf("OK %s\n",logo_types[1]);
    
    return 0;
}
