//
//  main.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>

void print_string(char string[]){
    printf("%s\n",string);
}
int test_and_dec(int *number){
    if (*number>1) {
        *number -= 1;
        return 1;
    }else{
        return 0;
    }
}
int double_it(int number);

struct point{
    int x;
    int y;
};
struct line{
    struct point start;
    struct point end;
};

int main(int argc, const char * argv[]) {
    printf("Hello, World!\n");
    char string[]="take my name";
    print_string(string);
    printf("Double of 2 is %d\n",double_it(2));
    struct line my_line;
    my_line.start.x=10;
    my_line.start.y=0;
    my_line.end.x=20;
    my_line.end.y=10;
    int *your_line_x=&my_line.start.x;
    *your_line_x=20;
    printf("(%d,%d) => (%d,%d)\n",my_line.start.x,my_line.start.y,my_line.end.x,my_line.end.y);
    printf("my_line x [%d] and your_line x [%d]\n",my_line.start.x,*your_line_x);
    int number;
    printf("Input any number: ");
    scanf("%d",&number);
    while (test_and_dec(&number)) {
        printf("Number was greater than zero so decremeneted and now value is [%d]\n",number);
    }
    return 0;
}

int double_it(int number){
    return 2*number;
}
