//
//  function_pointer.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>
#include <stdlib.h>

int times2(int x){
    return x*2;
}
int square(int x){
    return x*x;
}

void array_double(int a[],int len){
    for (int i=0; i<len; i++) {
        a[i]=times2(a[i]);
    }
}
void array_apply(int a[],int len,int (*fp)(int)){
    for (int i=0; i<len; i++) {
        a[i]=(*fp)(a[i]);
    }
}
void print_array(char *message,int a[],int len){
    printf("%s: [",message);
    for (int i=0; i<5; i++) {
        if (i<=3) {
            printf("%d,",a[i]);
        }else{
            printf("%d]\n",a[i]);
        }
    }
}

struct dog{
    char *name;
    void (*bark)(void);
};
void arf(){
    printf("arf\n");
}
void woof(){
    printf("woof\n");
}

int main(int argc, char **argv){
    int a[]={1,5,2,4,10};
    print_array("Array: ", a, 5);
    array_apply(a, 5, &times2);
    print_array("Array after each element is 2 times: ", a, 5);
    array_apply(a, 5, &square);
    print_array("Array after each element squared: ", a, 5);
    
    struct dog *one=(struct dog*)malloc(sizeof(struct dog));
    one->name="Jhon";
    one->bark=arf;
    one->bark();
}
